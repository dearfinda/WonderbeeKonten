const webRoutes = require('./web')
const router = require('express').Router()

router.use('/', webRoutes)

module.exports = router
