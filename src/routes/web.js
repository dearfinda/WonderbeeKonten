const testerController = require('../controllers/web/TesterController')
const cookieSession = require('../middlewares/cookie-session')
const csrfProtection = require('../middlewares/csrf-protection')
const router = require('express').Router()

router.use(cookieSession)
router.use(csrfProtection)

router.post('/beta-tester', testerController.store)
router.get('/beta-tester', testerController.showForm)
router.get('/beta-tester/registered', testerController.showRegistered)

module.exports = router
