const Joi = require('joi')

exports.validateStore = function (data) {
  return new Promise(function (resolve, reject) {
    let result = Joi.validate(data, {
      contact: Joi.string().required(),
      comment: Joi.string().allow('')
    })

    if (result.error) {
      reject(new Error(result.error.details[0].message))
    }

    result = Joi.validate(data.contact, [Joi.string().regex(/^[0-9]+$/).min(10), Joi.string().email()])

    if (result.error) {
      reject(new Error('Not a valid email/phone number'))
    }

    resolve()
  })
}
