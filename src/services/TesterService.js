const repo = require('../repositories/TesterRepo')

module.exports = {
  async store (data) {
    try {
      let tester = await repo.find(data.contact)

      if (!tester) {
        tester = await repo.create(data)
      }

      const total = await repo.count()

      return {
        tester: tester,
        total: total
      }
    } catch (error) {
      throw error
    }
  }
}
