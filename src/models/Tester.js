const mongoose = require('mongoose')

const testerSchema = new mongoose.Schema({
  contact: {
    type: String,
    required: true
  },
  comment: {
    type: String,
    default: ''
  }
})

const Tester = mongoose.model('Tester', testerSchema)

module.exports = Tester
