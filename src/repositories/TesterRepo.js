const Tester = require('../models/Tester')

module.exports = {
  async create (data) {
    try {
      let tester = new Tester(data)
      tester = await tester.save()

      return tester
    } catch (error) {
      throw error
    }
  },

  async find (contact) {
    try {
      const tester = await Tester.findOne({ contact: contact })

      return tester
    } catch (error) {
      throw error
    }
  },

  async count () {
    try {
      const total = await Tester.count()

      return total
    } catch (error) {
      throw error
    }
  }
}
