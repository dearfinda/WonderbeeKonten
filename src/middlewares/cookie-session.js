const config = require('config')
const cookieSession = require('cookie-session')

module.exports = cookieSession({
  secret: config.get('secret.session')
})
