const http = require('http')

class HttpServer {
  constructor (app) {
    this.server = http.createServer(app)
  }

  listen (port) {
    this.server.listen(port)
    console.log(`Server listening on port ${port}.`)
  }
}

module.exports = HttpServer
