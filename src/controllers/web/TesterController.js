const validator = require('../../validators/TesterValidator')
const service = require('../../services/TesterService')

module.exports = {
  showForm (req, res, next) {
    const data = {}

    if (req.session.tester && req.session.tester.error) {
      data.error = req.session.tester.error
    }

    req.session.tester = {}
    data.csrfToken = req.csrfToken()

    res.render('tester-form', data)
  },

  showRegistered (req, res, next) {
    if (req.session.tester.success) {
      let data = req.session.tester.success

      req.session.tester = {}
      data.csrfToken = req.csrfToken()

      res.render('tester-registered', data)
    } else {
      res.redirect('/beta-tester')
    }
  },

  store (req, res, next) {
    const data = {
      contact: req.body.contact,
      comment: req.body.comment
    }

    validator.validateStore(data)
      .then(() => {
        return service.store(data)
      })
      .then(result => {
        req.session.tester.success = result

        res.redirect('/beta-tester/registered')
      })
      .catch(error => {
        req.session.tester.error = {
          message: error.message
        }

        res.redirect('/beta-tester#tester-form')
      })
  }
}
