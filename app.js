const middlewares = require('./src/middlewares')
const routes = require('./src/routes')
const HttpServer = require('./src/utils/HttpServer')
const nuxtConfig = require('./nuxt.config')
const config = require('config')
const mongoose = require('mongoose')
const { Nuxt, Builder } = require('nuxt')
const nuxt = new Nuxt(nuxtConfig)
const app = require('express')()

app.set('view engine', 'pug')

app.use(middlewares)

app.use(routes)

app.use(nuxt.render)

const server = new HttpServer(app)

if (nuxtConfig.dev) {
  new Builder(nuxt).build()
    .then(() => {
      return mongoose.connect(config.get('db'))
    })
    .then(() => {
      console.log('Database connected.')
      server.listen(config.get('port'))
    })
    .catch((error) => {
      console.error(error)
      process.exit(1)
    })
} else {
  mongoose.connect(config.get('db'))
    .then(function () {
      console.log('Database connected.')
      server.listen(config.get('port'))
    })
    .catch((error) => {
      console.error(error)
      process.exit(1)
    })
}
